export function fetchPosts() {
  return function(dispatch) {
    fetch(`/go`)
      .then(res => res.json())
      .then(posts => dispatch({
        type: 'FETCH_POSTS',
        data: posts
      }));
  }
}

export function fetchAdvertise() {
  return function(dispatch) {
    fetch(`/home`)
      .then(res => res.json())
      .then(posts => dispatch({
        type: 'FETCH_ADS',
        data: posts
      }));
  }
}

export function addPost(post) {
  return fetch('/login',{
    method: 'POST',
    type: 'ADD_POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      firstParam: 'yourValue',
      secondParam: 'yourOtherValue',
    }),
    data: {
      name: post.name,
      phno: post.phno,
      emailid: post.emailid,
      location: post.location,
      address: post.address,
      password: post.password
    }
  });
}