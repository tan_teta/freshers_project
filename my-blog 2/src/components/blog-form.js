import React, { Component } from 'react';
import { connect } from 'react-redux';

import { addPost } from '../actions';

class BlogForm extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      phno: '',
      emailid: '',
      location:'',
      address:'',
      password:''
    };

    this.onChange = this.onChange.bind(this);
    this.formSubmit = this.formSubmit.bind(this);
  }

  onChange(e) {
    this.setState({[e.target.name]: e.target.value});
    
  }

  formSubmit(e) {
    debugger;
    e.preventDefault();
    this.props.addPost(this.state);
    this.setState({
      name: '',
      phno: '',
      emailid: '',
      location:'',
      address:'',
      password:''

    });
  }

  render() {
    return (
      <form onSubmit={ this.formSubmit }>
        Name: <input type="text" name="name" value={ this.state.name } onChange={ this.onChange }/><br/>
        Phone Number: <input type="number" name="phno" value={ this.state.phno } onChange={ this.onChange }/><br/>
        E-mail ID: <input type="textarea" name="emailid" value={ this.state.emailid } onChange={ this.onChange }/><br/>
        Location: <input type="textarea" name="location" value={ this.state.location } onChange={ this.onChange }/><br/>
        Address: <input type="textarea" name="address" value={ this.state.address } onChange={ this.onChange }/><br/>
        Password: <input type="password" name="password" value={ this.state.password } onChange={ this.onChange }/><br/>
     
        <button type="submit">Submit</button>
      </form>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    addPost: (post) => dispatch(addPost(post))
  };
}

export default connect(null, mapDispatchToProps)(BlogForm);