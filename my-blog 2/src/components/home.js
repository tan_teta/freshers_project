import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchAdvertise } from '../actions';

class Home extends Component {
  constructor() {
    super();
    // this.state = {
    //   posts: [
    //     {title: 'a', content: 'content-a'},
    //     {title: 'b', content: 'content-b'},
    //     {title: 'c', content: 'content-c'},
    //     {title: 'd', content: 'content-d'}
    //   ]
    // };
  }

  componentDidMount() {
    this.props.fetchAdvertise();
  }

  render() {
    //console.log(this.props.posts);

    // const { posts } = this.props;
    if(this.props.posts && this.props.posts.posts && this.props.posts.posts.length >=0) {
      return (
        <ul>
          {
            this.props.posts.posts.map(function(post) {
              return (
                <li>
                 
                  <p>Name - { post.sellerid }</p>
                  <p>Phone_number - { post.category }</p>
                  <p>Location - { post.location }</p>
                  <p>Address - { post.advertisement }</p>
                 
                </li>
              )
            })
          }
        </ul>
      );
    } else {
      return (
        <h3>Loading...</h3>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    posts: state.posts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchAdvertise: () => dispatch(fetchAdvertise())
  }; 
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);