import React, { Component } from 'react';
import BlogForm from './components/blog-form';
import List from './components/list';
import Login from './components/login';
import Home from './components/home';
import Header from './header'
import Main from './main'
import Footer from './footer'

import store from './store';
import { Provider } from 'react-redux';

// const store = createStore(() => [], {}, applyMiddleware());

class App extends Component {
  render() {
    return (
      <Provider store={ store }>
        <div className="App">
          <br/>
          <Header/>
          <br/>
          <br/>
          <br/>
          <Main />
          <br/>
          <br/>
          <Footer/>
         
          <br/>
          
          
        </div>
      </Provider>
    );
  }
}

export default App;
